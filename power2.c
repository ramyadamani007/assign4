#include <stdio.h>
int power(int, int);
int main()
{
    int b, p, r;
    printf("Enter base and power.\n");
    scanf("%d %d", &b, &p);
    r = power(b, p);
    printf("%d^%d = %d\n", b, p, r);
    return 0;
}

int power(int b, int p) 
{
    if (p != 0)
        return (b * power(b,p-1));
    else
        return 1;
}
